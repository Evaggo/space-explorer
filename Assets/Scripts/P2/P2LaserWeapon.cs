﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2LaserWeapon : Weapon
{
    public GameObject laserBullet;
    public float cadencia;

    public AudioSource audioSource;

    public override float GetCadencia()
    {
        return cadencia;
    }

    public override void Shoot()
    {
        Instantiate(laserBullet, this.transform.position, Quaternion.Euler(0, 0, 180), null);

        //Play audio
        audioSource.Play();
    }
}
