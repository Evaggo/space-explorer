﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class P2ShipInput : MonoBehaviour
{
    private Vector2 axis;

    public P2Behaviour P2ship;


    // Update is called once per frame
    void Update()
    {
        axis.x = Input.GetAxis("HorizontalP2");
        axis.y = Input.GetAxis("VerticalP2");

        if (Input.GetButton("Fire2"))
        {
            P2ship.Shoot();
        }

        P2ship.ActualizaDatosInput(axis);
    }
}
