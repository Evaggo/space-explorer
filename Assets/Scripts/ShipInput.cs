﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ShipInput : MonoBehaviour
{
    private Vector2 axis;

    public PlayerBehaviour ship;
    public Background bg;
    public Background llamas1;
    public Background llamas2;


    // Update is called once per frame
    void Update () {
        Scene currentScene = SceneManager.GetActiveScene();
        string sceneName = currentScene.name;

        axis.x = Input.GetAxis ("Horizontal");
        axis.y = Input.GetAxis ("Vertical");

        if(Input.GetButton("Fire1")){
            ship.Shoot();
        }

        if (sceneName == "Satan") {
            llamas1.SetVelocity(axis.x);
            llamas2.SetVelocity(axis.x);
        }

        ship.ActualizaDatosInput(axis);
        bg.SetVelocity(axis.x);
    }

}
