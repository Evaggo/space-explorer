﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOver : MonoBehaviour
{
    public AudioSource buttonSound;

    public void Replay()
    {
        buttonSound.Play();
        SceneManager.LoadScene("LevelSelection");
    }

    public void ToMenu()
    {
        buttonSound.Play();
        SceneManager.LoadScene("MainMenu");
    }

    public void Exit()
    {
        buttonSound.Play();
        Application.Quit();
    }
}
