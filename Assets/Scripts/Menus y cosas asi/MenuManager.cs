﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class MenuManager : MonoBehaviour
{
    public AudioSource buttonSound; 

    public void PulsaPlay(){
        buttonSound.Play();
        SceneManager.LoadScene("Game");
    }
    
    public void SinglePlayer(){
        buttonSound.Play();
        SceneManager.LoadScene("ShipSelection");
        PlayerPrefs.SetInt("GameMode", 0);
    }

    public void Cooperative(){
        buttonSound.Play();
        SceneManager.LoadScene("SelectionCooperativo");
        PlayerPrefs.SetInt("GameMode", 1);
    }

    public void Duel(){
        buttonSound.Play();
        PlayerPrefs.SetInt("GameMode", 2);
        SceneManager.LoadScene("SelectionCooperativo");
    }

    public void HighScore()
    {
        buttonSound.Play();
        SceneManager.LoadScene("HighScores");
    }

    public void PulsaComic()
    {
        buttonSound.Play();
        SceneManager.LoadScene("Comic");
    }

    public void Credits()
    {
        buttonSound.Play();
        SceneManager.LoadScene("Credits");
    }

    public void ReturnMenu()
    {
        buttonSound.Play();
        SceneManager.LoadScene("MainMenu");
    }

    public void PulsaExit(){
        Application.Quit();
    }
}
