﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsManager : MonoBehaviour
{
    public AudioSource buttonSound;

    public void GoBack()
    {
        buttonSound.Play();
        SceneManager.LoadScene("MainMenu");
    }
}
