﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class LevelManager : MonoBehaviour
{
    public AudioSource buttonSound;

    public void Mercadona()
    {
        PlayerPrefs.SetInt("GameLevel", 1);
        buttonSound.Play();
        SceneManager.LoadScene("Game");
    }

    public void Radioctive()
    {
        PlayerPrefs.SetInt("GameLevel", 2);
        buttonSound.Play();
        SceneManager.LoadScene("Radioactivo");
    }

    public void Toys()
    {
        PlayerPrefs.SetInt("GameLevel", 3);
        buttonSound.Play();
        SceneManager.LoadScene("Toys");
    }

    public void Disney()
    {
        PlayerPrefs.SetInt("GameLevel", 4);
        buttonSound.Play();
        SceneManager.LoadScene("Disney");
    }

    public void Hell()
    {
        PlayerPrefs.SetInt("GameLevel", 5);
        buttonSound.Play();
        SceneManager.LoadScene("Satan");
    }
}

