﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class ShipSelectionCoop : MonoBehaviour
{
    public GameObject gummi1;
    public GameObject yellow1;
    public GameObject red1;
    public GameObject blue1;
    public GameObject green1;
    public GameObject pink1;
    public GameObject purple1;

    public GameObject gummi2;
    public GameObject yellow2;
    public GameObject red2;
    public GameObject blue2;
    public GameObject green2;
    public GameObject pink2;
    public GameObject purple2;

    public AudioSource buttonSound;

    private int Mode;

    private void Start() {
        PlayerPrefs.SetInt("ShipSelected", 0);
        Mode = PlayerPrefs.GetInt("GameMode");
    }

    public void GummiShip1()
    {
        buttonSound.Play();
        gummi1.SetActive(true);
        yellow1.SetActive(false);
        red1.SetActive(false);
        blue1.SetActive(false);
        green1.SetActive(false);
        pink1.SetActive(false);
        purple1.SetActive(false);

        PlayerPrefs.SetInt("ShipSelected", 0);
    }

    public void YellowShip1()
    {
        buttonSound.Play();
        gummi1.SetActive(false);
        yellow1.SetActive(true);
        red1.SetActive(false);
        blue1.SetActive(false);
        green1.SetActive(false);
        pink1.SetActive(false);
        purple1.SetActive(false);

        PlayerPrefs.SetInt("ShipSelected", 1);
    }

    public void RedShip1()
    {
        buttonSound.Play();
        gummi1.SetActive(false);
        yellow1.SetActive(false);
        red1.SetActive(true);
        blue1.SetActive(false);
        green1.SetActive(false);
        pink1.SetActive(false);
        purple1.SetActive(false);

        PlayerPrefs.SetInt("ShipSelected", 2);
    }

    public void BlueShip1()
    {
        buttonSound.Play();
        gummi1.SetActive(false);
        yellow1.SetActive(false);
        red1.SetActive(false);
        blue1.SetActive(true);
        green1.SetActive(false);
        pink1.SetActive(false);
        purple1.SetActive(false);

        PlayerPrefs.SetInt("ShipSelected", 3);
    }

    public void GreenShip1()
    {
        buttonSound.Play();
        gummi1.SetActive(false);
        yellow1.SetActive(false);
        red1.SetActive(false);
        blue1.SetActive(false);
        green1.SetActive(true);
        pink1.SetActive(false);
        purple1.SetActive(false);

        PlayerPrefs.SetInt("ShipSelected", 4);
    }

    public void PinkShip1()
    {
        buttonSound.Play();
        gummi1.SetActive(false);
        yellow1.SetActive(false);
        red1.SetActive(false);
        blue1.SetActive(false);
        green1.SetActive(false);
        pink1.SetActive(true);
        purple1.SetActive(false);

        PlayerPrefs.SetInt("ShipSelected", 5);
    }

    public void PurpleShip1()
    {
        buttonSound.Play();
        gummi1.SetActive(false);
        yellow1.SetActive(false);
        red1.SetActive(false);
        blue1.SetActive(false);
        green1.SetActive(false);
        pink1.SetActive(false);
        purple1.SetActive(true);

        PlayerPrefs.SetInt("ShipSelected", 6);
    }

    public void GummiShip2()
    {
        buttonSound.Play();
        gummi2.SetActive(true);
        yellow2.SetActive(false);
        red2.SetActive(false);
        blue2.SetActive(false);
        green2.SetActive(false);
        pink2.SetActive(false);
        purple2.SetActive(false);

        PlayerPrefs.SetInt("ShipSelected2", 0);
    }

    public void YellowShip2()
    {
        buttonSound.Play();
        gummi2.SetActive(false);
        yellow2.SetActive(true);
        red2.SetActive(false);
        blue2.SetActive(false);
        green2.SetActive(false);
        pink2.SetActive(false);
        purple2.SetActive(false);

        PlayerPrefs.SetInt("ShipSelected2", 1);
    }

    public void RedShip2()
    {
        buttonSound.Play();
        gummi2.SetActive(false);
        yellow2.SetActive(false);
        red2.SetActive(true);
        blue2.SetActive(false);
        green2.SetActive(false);
        pink2.SetActive(false);
        purple2.SetActive(false);

        PlayerPrefs.SetInt("ShipSelected2", 2);
    }

    public void BlueShip2()
    {
        buttonSound.Play();
        gummi2.SetActive(false);
        yellow2.SetActive(false);
        red2.SetActive(false);
        blue2.SetActive(true);
        green2.SetActive(false);
        pink2.SetActive(false);
        purple2.SetActive(false);

        PlayerPrefs.SetInt("ShipSelected2", 3);
    }

    public void GreenShip2()
    {
        buttonSound.Play();
        gummi2.SetActive(false);
        yellow2.SetActive(false);
        red2.SetActive(false);
        blue2.SetActive(false);
        green2.SetActive(true);
        pink2.SetActive(false);
        purple2.SetActive(false);

        PlayerPrefs.SetInt("ShipSelected2", 4);
    }

    public void PinkShip2()
    {
        buttonSound.Play();
        gummi2.SetActive(false);
        yellow2.SetActive(false);
        red2.SetActive(false);
        blue2.SetActive(false);
        green2.SetActive(false);
        pink2.SetActive(true);
        purple2.SetActive(false);

        PlayerPrefs.SetInt("ShipSelected2", 5);
    }

    public void PurpleShip2()
    {
        buttonSound.Play();
        gummi2.SetActive(false);
        yellow2.SetActive(false);
        red2.SetActive(false);
        blue2.SetActive(false);
        green2.SetActive(false);
        pink2.SetActive(false);
        purple2.SetActive(true);

        PlayerPrefs.SetInt("ShipSelected2", 6);
    }

    public void Next()
    {
        buttonSound.Play();
        if (Mode == 1) {
            SceneManager.LoadScene("LevelSelection");
        } else if (Mode == 2) {
            SceneManager.LoadScene("Duel Mode");
        }

    }

    public void BackMenu()
    {
        buttonSound.Play();
        SceneManager.LoadScene("MainMenu");

    }
}
