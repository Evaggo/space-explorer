﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LogoManager : MonoBehaviour
{
    public void Awake()
    {
        StartCoroutine(GoMenu());
    }

    public void Update()
    {
        if (Input.anyKey) {
            SceneManager.LoadScene("MainMenu");
        }
    }

    IEnumerator GoMenu()
    {
        yield return new WaitForSeconds(6f);
        SceneManager.LoadScene("MainMenu");
    }
}
