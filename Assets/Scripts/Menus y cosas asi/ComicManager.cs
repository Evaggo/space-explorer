﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class ComicManager : MonoBehaviour
{
    public AudioSource buttonSound;

    public void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            buttonSound.Play();
            SceneManager.LoadScene("MainMenu");
        }
    }
}
