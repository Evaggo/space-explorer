﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class HighScores : MonoBehaviour
{
    public Text MercadonaTxt;
    private int MercadonaHS;
    private int MercadonaScore;

    public Text RadioactiveTxt;
    private int RadioactiveHS;
    private int RadioactiveScore;

    public Text ToysTxt;
    private int ToysHS;
    private int ToysScore;

    public Text DisneyTxt;
    private int DisneyHS;
    private int DisneyScore;

    public Text HellTxt;
    private int HellHS;
    private int HellScore;

    public AudioSource buttonSound;

    public void Awake()
    {
        //Mercadona HighScore
        MercadonaScore = PlayerPrefs.GetInt("MercadonaScore");
        MercadonaHS = PlayerPrefs.GetInt("MercadonaHS");

        if (MercadonaScore > MercadonaHS)
        {
            PlayerPrefs.SetInt("MercadonaHS", MercadonaScore);
            MercadonaHS = PlayerPrefs.GetInt("MercadonaHS");
        }

        //Radioactive HighScore
        RadioactiveScore = PlayerPrefs.GetInt("RadioactiveScore");
        RadioactiveHS = PlayerPrefs.GetInt("RadioactiveHS");

        if (RadioactiveScore > RadioactiveHS)
        {
            PlayerPrefs.SetInt("RadioactiveHS", RadioactiveScore);
            RadioactiveHS = PlayerPrefs.GetInt("RadioactiveHS");
        }

        //Toys HighScore
        ToysScore = PlayerPrefs.GetInt("ToysScore");
        ToysHS = PlayerPrefs.GetInt("ToysHS");

        if (ToysScore > ToysHS)
        {
            PlayerPrefs.SetInt("ToysHS", ToysScore);
            ToysHS = PlayerPrefs.GetInt("ToysHS");
        }

        //Disney HighScore
        DisneyScore = PlayerPrefs.GetInt("DisneyScore");
        DisneyHS = PlayerPrefs.GetInt("DisneyHS");

        if (DisneyScore > DisneyHS)
        {
            PlayerPrefs.SetInt("DisneyHS", DisneyScore);
            DisneyHS = PlayerPrefs.GetInt("DisneyHS");
        }

        //Hell HighScore
        HellScore = PlayerPrefs.GetInt("HellScore");
        HellHS = PlayerPrefs.GetInt("HellHS");

        if (HellScore > HellHS)
        {
            PlayerPrefs.SetInt("HellHS", HellScore);
            HellHS = PlayerPrefs.GetInt("HellHS");
        }
    }
    
    public void Start()
    {
        //MercadonaHS = 50;
        MercadonaTxt.text = MercadonaHS.ToString("000000");
        RadioactiveTxt.text = RadioactiveHS.ToString("000000");
        ToysTxt.text = ToysHS.ToString("000000");
        DisneyTxt.text = DisneyHS.ToString("000000");
        HellTxt.text = HellHS.ToString("000000");
    }
    public void BackToMenu()
    {
        buttonSound.Play();
        SceneManager.LoadScene("MainMenu");
    }
}
