﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{
    [SerializeField] GameObject[] lives;
    [SerializeField] Text score;
    [SerializeField] GameObject graphics;
    int Mode;
    public static int scoreInt=0;
    private int currentLives = 3;

    public void Awake()
    {
        Mode = PlayerPrefs.GetInt("GameMode");
        if (Mode == 1) {
            graphics.gameObject.SetActive(true);
        }
    }


    // Start is called before the first frame update
    void Start()
    {
        scoreInt = 0;
        currentLives = 3;
        score.text = scoreInt.ToString("000000");
        //score.text = scoreInt.ToString();
    }

    public void AddScore(int value){
        scoreInt+=value;
        score.text = scoreInt.ToString("000000");
    }

    public void LoseLife()
    {
        currentLives--;
        if (currentLives >= 0)
        {
            lives[currentLives].SetActive(false);
        }
    }
    public void WinLife()
    {
        currentLives++;
        if (currentLives > 0)
        {
            lives[currentLives-1].SetActive(true);
        }
    }

}
