﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class ShipSelection : MonoBehaviour
{
    public GameObject gummi;
    public GameObject yellow;
    public GameObject red;
    public GameObject blue;
    public GameObject green;
    public GameObject pink;
    public GameObject purple;

    public AudioSource buttonSound;

    private void Start()
    {
        PlayerPrefs.SetInt("ShipSelected", 0);
        PlayerPrefs.SetInt("GameMode", 0);
    }

    public void GummiShip(){
        buttonSound.Play();
        gummi.SetActive(true);
        yellow.SetActive(false);
        red.SetActive(false);
        blue.SetActive(false);
        green.SetActive(false);
        pink.SetActive(false);
        purple.SetActive(false);
        PlayerPrefs.SetInt("ShipSelected", 0);
    }

    public void YellowShip(){
        buttonSound.Play();
        gummi.SetActive(false);
        yellow.SetActive(true);
        red.SetActive(false);
        blue.SetActive(false);
        green.SetActive(false);
        pink.SetActive(false);
        purple.SetActive(false);
        PlayerPrefs.SetInt("ShipSelected", 1);
    }

    public void RedShip(){
        buttonSound.Play();
        gummi.SetActive(false);
        yellow.SetActive(false);
        red.SetActive(true);
        blue.SetActive(false);
        green.SetActive(false);
        pink.SetActive(false);
        purple.SetActive(false);
        PlayerPrefs.SetInt("ShipSelected", 2);
    }

    public void BlueShip(){
        buttonSound.Play();
        gummi.SetActive(false);
        yellow.SetActive(false);
        red.SetActive(false);
        blue.SetActive(true);
        green.SetActive(false);
        pink.SetActive(false);
        purple.SetActive(false);
        PlayerPrefs.SetInt("ShipSelected", 3);
    }

    public void GreenShip(){
        buttonSound.Play();
        gummi.SetActive(false);
        yellow.SetActive(false);
        red.SetActive(false);
        blue.SetActive(false);
        green.SetActive(true);
        pink.SetActive(false);
        purple.SetActive(false);
        PlayerPrefs.SetInt("ShipSelected", 4);
    }

    public void PinkShip(){
        buttonSound.Play();
        gummi.SetActive(false);
        yellow.SetActive(false);
        red.SetActive(false);
        blue.SetActive(false);
        green.SetActive(false);
        pink.SetActive(true);
        purple.SetActive(false);
        PlayerPrefs.SetInt("ShipSelected", 5);
    }

    public void PurpleShip(){
        buttonSound.Play();
        gummi.SetActive(false);
        yellow.SetActive(false);
        red.SetActive(false);
        blue.SetActive(false);
        green.SetActive(false);
        pink.SetActive(false);
        purple.SetActive(true);
        PlayerPrefs.SetInt("ShipSelected", 6);
    }

    public void Next() {
        buttonSound.Play();
        SceneManager.LoadScene("LevelSelection");
    }

    public void BackMenu()
    {
        buttonSound.Play();
        SceneManager.LoadScene("MainMenu");

    }
}
