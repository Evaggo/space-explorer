﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class NextLevelManager : MonoBehaviour
{
    public AudioSource buttonSound;
    int sceneNum;

    public void Next()
    {
        sceneNum = PlayerPrefs.GetInt("SceneSelected");
        buttonSound.Play();
        if (sceneNum == 1)
        {
            SceneManager.LoadScene("Radioactivo");
        }
        else if (sceneNum == 2)
        {
            SceneManager.LoadScene("Toys");
        }
        else if (sceneNum == 3)
        {
            SceneManager.LoadScene("Disney");
        }
        else if (sceneNum == 4)
        {
            SceneManager.LoadScene("Satan");
        }
        else if (sceneNum == 5) {
            SceneManager.LoadScene("Credits");
        }
    }

    public void Quit()
    {
        buttonSound.Play();
        SceneManager.LoadScene("MainMenu");
    }
}
