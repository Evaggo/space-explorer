﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class WinManager : MonoBehaviour
{
    public AudioSource buttonSound;
    public void ToMenu()
    {
        buttonSound.Play();
        SceneManager.LoadScene("MainMenu");
    }

    public void Exit()
    {
        buttonSound.Play();
        Application.Quit();
    }
}
