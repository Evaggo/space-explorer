﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Parallax_fondo : MonoBehaviour
{
    /*private float length, startpos;
    private Sprite sprite;
    public GameObject camera;
    public float parallaxEffect;

    // Start is called before the first frame update
    void Start()
    {
        startpos = transform.position.x;
        length = GetComponent<SpriteRenderer>().bounds.size.x;
        sprite = GetComponent<SpriteRenderer>().sprite;
    }

    // Update is called once per frame
    void Update()
    {
        float temp = (camera.transform.position.x * (1-parallaxEffect));
        float dist = (camera.transform.position.x * parallaxEffect);
        transform.position = new Vector3(startpos + dist, transform.position.y, transform.position.z);

        if (temp > startpos + length) {
            startpos += length;
        } else if (temp < startpos - length) {
            startpos -= length;
        }

    }*/

    private Material mat;
    public float speed = 0;

    void Update() {
        mat = GetComponent<Renderer>().material;
        mat.mainTextureOffset = new Vector2(Time.time * speed, 0f);
    }


}
