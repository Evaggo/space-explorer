﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Boss : MonoBehaviour
{
    private Vector2 speed = new Vector2(0, 0);
    public GameObject laserBullet;
    public GameObject graphics;
    [SerializeField] BoxCollider2D collider;
    [SerializeField] ParticleSystem ps;
    [SerializeField] AudioSource audioSource;
    [SerializeField] AudioSource audioDamage;
    public GameObject WinCanvas;
    int lives=10;
    private ScoreManager sm;
    Scene m_Scene;
    string sceneName;
    int sceneNum;

    public int puntuacion;

    private void Start()
    {
        sm = (GameObject.Find("ScoreCanvas")).GetComponent<ScoreManager>();
        StartCoroutine(BossBehaviour());

        m_Scene = SceneManager.GetActiveScene();
        sceneName = m_Scene.name;

        if (sceneName == "Game")
        {
            PlayerPrefs.SetInt("SceneSelected", 1);
        }
        else if (sceneName == "Radioactivo")
        {
            PlayerPrefs.SetInt("SceneSelected", 2);
        }
        else if (sceneName == "Toys")
        {
            PlayerPrefs.SetInt("SceneSelected", 3);
        }
        else if (sceneName == "Disney")
        {
            PlayerPrefs.SetInt("SceneSelected", 4);
            }
        else if (sceneName == "Satan")
        {
            PlayerPrefs.SetInt("SceneSelected", 5);
        }


    }

    void Update()
    {
        transform.Translate(speed * Time.deltaTime);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Bullet" & (lives>0))
        {
            Damage();
        }
        else if (other.tag == "Bullet" & (lives<=0)) {
            StartCoroutine(DestroyShip());
        }
    }

    private void Damage()
    {
        audioDamage.Play();
        lives--;
    }

    IEnumerator BossBehaviour()
    {
        while (true)
        {
            //dispara
            Instantiate(laserBullet, this.transform.position, Quaternion.Euler(0, 0, 180), null);
            speed.y = -1f;

            yield return new WaitForSeconds(1.0f);
            //dispara
            Instantiate(laserBullet, this.transform.position, Quaternion.Euler(0, 0, 180), null);
            speed.y = 1f;
            yield return new WaitForSeconds(1.0f);

        }
    }

    IEnumerator DestroyShip()
    {
        //Sumar puntos
        sm.AddScore(puntuacion);

        //Desactivo el grafico
        graphics.SetActive(false);

        //Elimino el BoxCollider2D
        collider.enabled = false;

        //Lanzo la partícula
        ps.Play();

        //Lanzo sonido de explosion
        audioSource.Play();

        //Me espero 1 segundo
        yield return new WaitForSeconds(1.0f);

        Destroy(this.gameObject);

        SceneManager.LoadScene("WinLevel");
    }
}
