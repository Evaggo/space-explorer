﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossManager : MonoBehaviour
{
    public GameObject boss;
    public float timeLaunchFormation;

    private float currentTime = 0;

    void Awake()
    {
        Instantiate(boss, new Vector3(7, Random.Range(-1, 1)), Quaternion.identity, this.transform);
    }
}