﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUpSpawner : MonoBehaviour
{
    public GameObject[] PowerUps;
    public float timeLaunch;

    private float currentTime = 0;

    void Awake()
    {
        StartCoroutine(AparicionPowerUp());
    }

    IEnumerator AparicionPowerUp()
    {
        int PowerUpActual = 0;
        while (true)
        {
            PowerUpActual = Random.Range(0, PowerUps.Length);

            Instantiate(PowerUps[PowerUpActual], new Vector3(10, Random.Range(-5, 5)), Quaternion.identity, this.transform);
            yield return new WaitForSeconds(timeLaunch);
        }
    }
}
