﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwentyBulletsPowerUp : MonoBehaviour
{
    Vector2 speed;
    Transform graphics;
    public GameObject powerUp;


    public void Awake()
    {
        speed.x = Random.Range(-5, -2);
        speed.y = Random.Range(-3, 3);
    }
    void Update()
    {
        transform.Translate(speed * Time.deltaTime);
        graphics.Rotate(0, 0, 100 * Time.deltaTime);
    }
}
