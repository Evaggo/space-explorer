﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LivesPowerUp : MonoBehaviour
{
    Vector2 speed;

    public void Awake()
    {
        speed.x = Random.Range(-5, -2);
        speed.y = Random.Range(-3, 3);
    }
    void Update()
    {
        transform.Translate(speed * Time.deltaTime);
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            Destroy(this.gameObject);
        }       
    }
}
